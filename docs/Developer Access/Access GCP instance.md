---
sidebar_position: 1
---
import ReactPlayer from 'react-player'

## Step 1 install ssh plugin in VSCODE
## Step 2 Configure Access to the GCP instance
## Step 3 Add GCP instance to Hosts
## Step 4 Access GCP instance

<div className="video__wrapper">
    <ReactPlayer className="video__player" controls height="100%" url="/remotessh.m4v" width="100%" />
</div>
